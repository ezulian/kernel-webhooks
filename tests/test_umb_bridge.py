"""Webhook interaction tests."""
import copy
from os import environ
from time import sleep
from unittest import TestCase
from unittest import mock

from gitlab.exceptions import GitlabListError
from pika.exceptions import AMQPError

from tests import fakes
from webhook import defs
from webhook import session
from webhook import umb_bridge
from webhook.common import get_arg_parser
from webhook.rh_metadata import Projects


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestUmbBridge(TestCase):
    """ Test Webhook class."""

    UMB_PUBLIC = {"event": {"target": "bug",
                            "routing_key": "bug.modify",
                            "time": "2021-05-13T08:38:22",
                            "action": "modify",
                            "user": {"real_name": "Bugzilla User",
                                     "id": 12345,
                                     "login": "buguser@example.com"
                                     },
                            "bug_id": 1234567,
                            "change_set": "140589.1620895102.43339"
                            },
                  "bug": {"priority": "high",
                          "is_private": False,
                          "last_change_time": "2021-05-13T08:34:23",
                          "keywords": ["Reproducer", "Triaged"],
                          "url": "",
                          "assigned_to": {"real_name": "Bugzilla User",
                                          "id": 12345,
                                          "login": "buguser@example.com"
                                          },
                          "whiteboard": "",
                          "id": 1234567,
                          "creation_time": "2021-01-13T13:49:16",
                          "resolution": "",
                          "classification": "Red Hat",
                          "alias": [],
                          "status": {"name": "POST", "id": 26},
                          "reporter": {"real_name": "Bugzilla User",
                                       "id": 12345,
                                       "login": "omosnace@redhat.com"
                                       },
                          "summary": "A bad bug >:(",
                          "severity": "medium",
                          "flags": [],
                          "version": {"name": "8.3", "id": 5979},
                          "component": {"name": "kernel", "id": 130532},
                          "product": {"name": "Red Hat Enterprise Linux 8", "id": 370},
                          }
                  }

    UMB_PRIVATE = {"event": {"time": "2021-05-13T13:23:38",
                             "rule_id": 123,
                             "bug_id": 5454545,
                             "target": "bug",
                             "routing_key": "bug.modify",
                             "user": {"real_name": "Bugzilla User",
                                      "id": 12345,
                                      "login": "buguser@example.com"
                                      },
                             "action": "modify",
                             "change_set": "133863.1620912223.24249"
                             }
                   }

    GL_MR_URL = f'{defs.GITFORGE}/group/project/-/merge_requests/3344'
    GL_MESSAGE = {'object_attributes': {'action': 'open',
                                        'description': 'hello',
                                        'url': GL_MR_URL
                                        },
                  'changes': {'description': {'previous': 'hi',
                                              'current': 'hello'
                                              }
                              },
                  'project': {'path_with_namespace': 'group/project'}
                  }

    def setUp(self):
        """Reset session.SESSION."""
        session.SESSION = None

    @mock.patch.dict(environ, {'BUGZILLA_EMAIL': 'kwf@example.com'})
    def test_process_umb_event(self):
        mock_send_queue = mock.Mock(send_function=None)
        mock_mapping_data = mock.Mock(bugs={}, jiras={})

        args = get_arg_parser('TEST').parse_args([])
        test_session = session.SessionRunner('umb_bridge', args, umb_bridge.MSG_HANDLERS)

        # No bug_id in body.
        with self.assertLogs('cki.webhook.umb_bridge', level='WARNING') as logs:
            umb_bridge.process_umb_event({'event': {}}, test_session,
                                         mapping_data=mock_mapping_data,
                                         send_queue=mock_send_queue)
            self.assertIn('No bug_id or user found in UMB event.', logs.output[-1])
            mock_send_queue.add_bug.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        body = copy.deepcopy(self.UMB_PRIVATE)
        bug_data = {2233445: {'group/project!123)', 'group/project2!334'}}
        mock_mapping_data.bugs.update(bug_data)

        # Bug is not in bug_data
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_umb_event(body, test_session,
                                         mapping_data=mock_mapping_data,
                                         send_queue=mock_send_queue)
            self.assertIn('Ignoring event for unknown bug 5454545.', logs.output[-1])
            mock_send_queue.add_bug.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Ignore BZ event generated by the KWF bot.
        body['event']['user']['login'] = environ['BUGZILLA_EMAIL']
        bug_data[5454545] = {'group/project!321', 'group1/project!111'}
        mock_mapping_data.bugs.update(bug_data)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_umb_event(body, test_session,
                                         mapping_data=mock_mapping_data,
                                         send_queue=mock_send_queue)
            self.assertIn('Ignoring event by bot user', logs.output[-1])
            mock_send_queue.add_bug.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Bug is in bug_data
        body = copy.deepcopy(self.UMB_PRIVATE)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_umb_event(body, test_session,
                                         mapping_data=mock_mapping_data,
                                         send_queue=mock_send_queue)
            msg = ('Event for bug 5454545 by buguser@example.com relevant to these MRs:'
                   f' {bug_data[5454545]}')
            self.assertIn(msg, logs.output[-1])
            self.assertEqual(mock_send_queue.add_bug.call_count, 2)
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

    def test_process_jira_event(self):
        """Adds relevant Jiras to the send_queue."""
        mock_send_queue = mock.Mock(send_function=None)
        mock_mapping_data = mock.Mock(bugs={}, jiras={})

        args = get_arg_parser('TEST').parse_args([])
        test_session = session.SessionRunner('umb_bridge', args, umb_bridge.MSG_HANDLERS)

        # No jissue in body.
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event({}, test_session,
                                          mapping_data=mock_mapping_data,
                                          send_queue=mock_send_queue)
            self.assertIn('Ignoring event for non-RHEL', logs.output[-1])
            mock_send_queue.add_jira.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Event from the bot user, ignored.
        event = {'user': {'name': defs.JIRA_BOT_ACCOUNTS[0]}}
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event(event, test_session,
                                          mapping_data=mock_mapping_data,
                                          send_queue=mock_send_queue)
            self.assertIn('Ignoring event by bot user', logs.output[-1])
            mock_send_queue.add_jira.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Jira is not in jira_data
        event = {'issue': {'key': 'RHEL-123'}, 'user': {'name': 'example_name'}}
        jira_data = {'RHEL-999': {'group/project!123)', 'group/project2!334'}}
        mock_mapping_data.jiras.update(jira_data)

        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event(event, test_session,
                                          mapping_data=mock_mapping_data,
                                          send_queue=mock_send_queue)
            self.assertIn('Ignoring event for non-RHEL', logs.output[-1])
            mock_send_queue.add_jira.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Jira is in jira_data, queue MRs.
        event = {'issue': {'key': 'RHEL-123'}, 'user': {'name': 'example_name'}}
        jira_data = {'RHEL-123': {'group/project!123)', 'group/project2!334'}}
        mock_mapping_data.jiras.update(jira_data)

        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event(event, test_session,
                                          mapping_data=mock_mapping_data,
                                          send_queue=mock_send_queue)
            msg = 'Event for RHEL-123 relevant to these MRs'
            self.assertIn(msg, logs.output[-1])
            self.assertEqual(mock_send_queue.add_jira.call_count, 2)
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

    def test_process_gitlab_mr(self):
        bug_data = mock.Mock()
        bug_data.return_value = {2233445: {'group/project!123)', 'group/project2!334'}}

        args = get_arg_parser('TEST').parse_args([])
        test_session = session.SessionRunner('umb_bridge', args, umb_bridge.MSG_HANDLERS)

        # MR has not changed.
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            msg = copy.deepcopy(self.GL_MESSAGE)
            msg['object_attributes']['action'] = 'modify'
            del msg['changes']['description']

            umb_bridge.process_gitlab_mr(msg, test_session, mapping_data=bug_data)
            self.assertIn('MR has not changed, ignoring event.', logs.output[-1])

        # MR is added to bug_data
        msg = copy.deepcopy(self.GL_MESSAGE)
        description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                       'Bugzilla: https://bugzilla.redhat.com/2233445')
        old_description = ('Bugzilla: https://bugzilla.redhat.com/5544332\n'
                           'Bugzilla: https://bugzilla.redhat.com/2233445')
        msg['changes']['description']['current'] = description
        msg['changes']['description']['previous'] = old_description
        msg['object_attributes']['description'] = description

        umb_bridge.process_gitlab_mr(msg, test_session, mapping_data=bug_data)
        bug_data.link_mr.assert_called_once()
        bug_data.unlink_mr.assert_called_once()

    def test_MappingData(self):
        mock_gl = fakes.FakeGitLab()
        mock_gl.auth()
        project_id = 123
        namespace = 'cool_group/good-project'
        routing_key = 'gitlab.com.cool_group.good-project.merge_request'
        mock_gl_project = mock_gl.add_project(project_id, namespace)

        mock_rh_project = mock.Mock(namespace=namespace)
        mock_rh_project.id = project_id
        mock_rh_projects = mock.Mock()
        mock_rh_projects.get_project_by_namespace.return_value = mock_rh_project

        mr1_description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                           'Bugzilla: https://bugzilla.redhat.com/5544332')
        mr1_url = f'{defs.GITFORGE}/cool_group/good-project/-/merge_requests/123'
        mock_gl_project.add_mr(1, actual_attributes={'description': mr1_description,
                                                     'web_url': mr1_url}
                               )

        mr2_description = ('Bugzilla: https://bugzilla.redhat.com/3456789\n'
                           'Bugzilla: https://bugzilla.redhat.com/7788665')
        mr2_url = f'{defs.GITFORGE}/cool_group/good-project/-/merge_requests/555'
        mock_gl_project.add_mr(2, actual_attributes={'description': mr2_description,
                                                     'web_url': mr2_url}
                               )

        mr3_description = ('Bugzilla: https://bugzilla.redhat.com/3456789\n'
                           'Bugzilla: https://bugzilla.redhat.com/2222222')
        mr3_url = f'{defs.GITFORGE}/cool_group/good-project2/-/merge_requests/333'
        mock_gl_project.add_mr(3, actual_attributes={'description': mr3_description,
                                                     'web_url': mr3_url}
                               )

        parser = get_arg_parser('TEST')
        args = parser.parse_args(f'--rabbitmq-routing-key {routing_key}'.split())
        test_session = session.SessionRunner('umb_bridge', args, umb_bridge.MSG_HANDLERS)
        test_session.gl_instance = mock_gl
        test_session.graphql = mock.Mock()
        test_session.rh_projects = mock_rh_projects

        # Confirm data is loaded and linked as expected.
        with self.assertLogs('cki.webhook.umb_bridge.MappingData', level='INFO') as logs:
            mapping_data = umb_bridge.MappingData(test_session)
            self.assertIn('MRs: 3, Bugs: 5, Jiras: 0.', logs.output[-1])
            expected_mrs = {'cool_group/good-project!123'}
            self.assertEqual(mapping_data.bugs[1234567], expected_mrs)
            expected_mrs = {'cool_group/good-project2!333'}
            self.assertEqual(mapping_data.bugs[2222222], expected_mrs)
            expected_mrs = {'cool_group/good-project2!333', 'cool_group/good-project!555'}
            self.assertEqual(mapping_data.bugs[3456789], expected_mrs)
            expected_mrs = {'cool_group/good-project!123'}
            self.assertEqual(mapping_data.bugs[5544332], expected_mrs)
            expected_mrs = {'cool_group/good-project!555'}
            self.assertEqual(mapping_data.bugs[7788665], expected_mrs)

        # UnLink MRs.
        mrpath = 'cool_group/good-project2!333'
        bug_list = [3456789, 5544332, 2222222]
        with self.assertLogs('cki.webhook.umb_bridge.MappingData', level='DEBUG') as logs:
            mapping_data.unlink_mr(mrpath, bug_list=bug_list)
            expected_mrs = {'cool_group/good-project!555'}
            self.assertEqual(mapping_data.bugs[3456789], expected_mrs)
            expected_mrs = {'cool_group/good-project!123'}
            self.assertEqual(mapping_data.bugs[5544332], expected_mrs)
            self.assertEqual(mapping_data.bugs[2222222], set())
            self.assertIn('cool_group/good-project2!333 removed from 3456789.', logs.output[-4])
            self.assertIn("5544332 can't remove cool_group/good-project2!333, not in item's set.",
                          logs.output[-3])
            self.assertIn('cool_group/good-project2!333 removed from 2222222.', logs.output[-2])
            self.assertIn('2222222 removed from mapping.', logs.output[-1])

        # Permissions error on project
        raised = False
        bad_list = mock.Mock()
        bad_list.side_effect = \
            GitlabListError(response_code=403, error_message='403 Forbidden')
        mock_gl_project.mergerequests.list = bad_list
        with self.assertLogs('cki.webhook.umb_bridge.MappingData', level='ERROR') as logs:
            try:
                mapping_data = umb_bridge.MappingData(test_session)
            except GitlabListError as err:
                if err.response_code != 403:
                    raised = True
            self.assertFalse(raised)
            self.assertIn("User 'example_user' does not have access", logs.output[-1])

        # Some other error.
        mock_gl_project.mergerequests.list.side_effect = \
            GitlabListError(response_code=123, error_message='123 bad error')
        try:
            mapping_data = umb_bridge.MappingData(test_session)
        except GitlabListError:
            raised = True
        self.assertTrue(raised)

    def test_get_mr_reference(self):
        web_url = f'{defs.GITFORGE}/cool_group/good-project/-/merge_requests/123'
        self.assertEqual(umb_bridge.get_mr_reference(web_url), 'cool_group/good-project!123')
        web_url = f'{defs.GITFORGE}/cool_group/subgroup/good-project1/-/merge_requests/555'
        self.assertEqual(umb_bridge.get_mr_reference(web_url),
                         'cool_group/subgroup/good-project1!555')

    def test_SendQueue(self):
        exchange = 'cki.exchange.test'
        route = 'cki.kwf.test.notice'
        send_queue = umb_bridge.SendQueue(exchange, route)

        paths = ['group/project!123', 'group/project!234', 'group/project!345']

        # Ensure nothing happens if send_function is not set.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='WARNING') as logs:
            send_queue.add_bug(paths[0])
            self.assertIn('send_function is not set!', logs.output[-1])
            self.assertEqual(len(send_queue.data), 0)

        # Nothing to send.
        send_function = mock.Mock()
        send_queue.send_function = send_function
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('Empty.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])

        # Add an item to the queue.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='INFO') as logs:
            send_queue.add_bug(paths[0])
            self.assertIn('Adding group/project!123 for bughook.', logs.output[-1])
            send_queue.add_bug(paths[1])
            self.assertIn('Adding group/project!234 for bughook.', logs.output[-1])
            send_queue.add_bug(paths[2])
            self.assertIn('Adding group/project!345 for bughook.', logs.output[-1])
            self.assertTrue((umb_bridge.MappingType.Bug, paths[0]) in send_queue.data)
            self.assertTrue((umb_bridge.MappingType.Bug, paths[1]) in send_queue.data)
            self.assertTrue((umb_bridge.MappingType.Bug, paths[2]) in send_queue.data)

        # Nothing to send as nothing has been in the queue more than SEND_DELAY.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('Nothing older than SEND_DELAY (1) in data.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])

        # Wait over SEND_DELAY and now we could send but send_function is not set.
        send_queue.send_function = None
        sleep(umb_bridge.SEND_DELAY)
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('send_function is not set!', logs.output[-1])
            self.assertEqual(len(send_queue.data), 3)

        # Now we can send.
        send_queue.send_function = send_function
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn(f'Sent {paths[0]} for bughook.', logs.output[-5])
            self.assertIn(f'Sent {paths[1]} for bughook.', logs.output[-4])
            self.assertIn(f'Sent {paths[2]} for bughook.', logs.output[-3])
            self.assertIn('Empty.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])
            self.assertEqual(len(send_queue.data), 0)
            self.assertEqual(send_function.call_count, 3)
            # Confirm the expected headers and data is given to the send_function.
            expected_headers = {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE,
                                'event_target_webhook': 'bughook'}
            for count, call in enumerate(send_function.call_args_list, start=0):
                self.assertEqual(call.kwargs['headers'], expected_headers)
                self.assertEqual(call.kwargs['data'], {'mrpath': paths[count]})

        # send_function raises an amqp exception?
        send_queue.add_bug(paths[0])
        send_function.side_effect = AMQPError('oh no!')
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='ERROR') as logs:
            exception_hit = False
            try:
                send_queue._send_message((umb_bridge.MappingType.Bug, paths[0]))
            except AMQPError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error sending message: oh no!', logs.output[-1])
        send_function.reset_mock(side_effect=True)

        # Test check_status.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            with mock.patch('sys.exit') as mock_exit:
                send_queue.add_bug(paths[0])
                send_queue.check_status()
                self.assertIn('Sender thread not running as expected.', logs.output[-2])
                self.assertIn(f'Sent {paths[0]} for bughook.', logs.output[-1])
                mock_exit.assert_called_with(1)

        # Test start.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            # Test start()
            self.assertFalse(send_queue._thread.is_alive())
            send_queue.start()
            self.assertIn('Starting sender thread.', logs.output[-1])
            self.assertTrue(send_queue._thread.is_alive())

    def test_route_keys_to_projects(self):
        route_keys = ['umb.bridge.cool.route',
                      'gitlab.com.redhat.rhel.src.kernel.rhel-8.merge_request',  # id 12345
                      'gitlab.com.redhat.rhel.src.kernel.rhel-8.note',
                      'gitlab.com.redhat.rhel.src.kernel.rhel-8-sandbox.merge_request'  # id 56789
                      ]

        # first value is the environment to run in, second value is the set of project ids we
        # expect to be returned by MappingData.projects.
        tests = [
            ('development', {12345, 56789}),
            ('production', {12345}),
            ('staging', {56789})
        ]

        parser = get_arg_parser('TEST')
        route_key_str = ' '.join(route_keys)
        args = parser.parse_args(['--rabbitmq-routing-key', route_key_str])

        for test in tests:
            mock_env = test[0]
            expected_project_ids = test[1]
            with self.subTest(environment=mock_env, expected_project_ids=expected_project_ids):
                with mock.patch.object(umb_bridge.MappingData, '_build_cache', mock.Mock()):
                    with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': mock_env}):
                        test_session = session.BaseSession('umb_bridge', args)
                        test_mapping_data = umb_bridge.MappingData(test_session)
                        results = test_mapping_data.projects
                        self.assertEqual(len(results), len(expected_project_ids))
                        result_ids = [project.id for project in results]
                        self.assertCountEqual(result_ids, expected_project_ids)

    @mock.patch('webhook.umb_bridge.SendQueue')
    @mock.patch('webhook.umb_bridge.MappingData')
    @mock.patch.object(session.SessionRunner, 'consume_messages')
    def test_main(self, mock_consume_messages, mock_MappingData, mock_SendQueue):
        args = ['--rabbitmq-routing-key',
                'umb.VirtualTopic.bugzilla gitlab.com.group.project.merge_request',
                '--rabbitmq-sender-exchange', 'cki.exchange.test',
                '--rabbitmq-sender-route', 'cki.route.test'
                ]

        umb_bridge.main(args)
        mock_SendQueue.assert_called_with('cki.exchange.test', 'cki.route.test')
        mock_SendQueue.return_value.start.assert_called_once()
        mock_MappingData.assert_called_once()
        mock_consume_messages.assert_called_once_with(mapping_data=mock_MappingData.return_value,
                                                      send_queue=mock_SendQueue.return_value)

    def test_MappingType(self):
        """Ensure that each MappingType member value corresponds to a known webhook name in yaml."""
        rh_projects = Projects()
        self.assertTrue(len(rh_projects.webhooks) > 1)

        for mapping_type in umb_bridge.MappingType:
            with self.subTest(mapping_type=mapping_type, webhooks=rh_projects.webhooks.keys()):
                self.assertIn(mapping_type.value, rh_projects.webhooks)
